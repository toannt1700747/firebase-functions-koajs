import * as functions from 'firebase-functions';
import apiHandler from './handlers/app';

exports.api = functions.https.onRequest(apiHandler.callback());
