const { initializeApp, cert } = require('firebase-admin/app');
const { getFirestore } = require('firebase-admin/firestore');
const serviceAccount = require('../config/db/serviceAccount.json');

initializeApp({
  credential: cert(serviceAccount),
});
const db = getFirestore();

const { faker } = require('@faker-js/faker');

const todoRef = db.collection('todos');

(async () => {
  const promise = [];
  for (i = 0; i < 10; i++) {
    promise.push(
      todoRef.add({
        title: faker.lorem.words(2),
        complete: faker.datatype.boolean(false),
        createdAt: new Date(),
      })
    );
  }
  await Promise.all(promise);
})();
