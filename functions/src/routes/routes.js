import Router from 'koa-router';
import * as todoMiddle from '../middleware/todoMiddleware';
import * as todoHandlers from '../handlers/todos/todoHandlers';

const router = new Router({
  prefix: '/api',
});
// Todos
// Add, get , update ,delete todos
router
  .post('/todos', todoMiddle.todoInputMiddleware, todoHandlers.addTodo)
  .get('/todos', todoHandlers.getTodos)
  .put('/todos', todoHandlers.updateMassTodo)
  .delete('/todos', todoHandlers.removeMassTodo);

// get, update , delete todo
router
  .get('/todo/:id', todoMiddle.todoFieldMiddleware, todoHandlers.getTodo)
  .put('/todo/:id', todoHandlers.updateTodo)
  .delete('/todo/:id', todoHandlers.removeTodo);

export default router;
